package com.gitlab.vincenthung.gogotech.lottery.service.impl;

import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.vincenthung.gogotech.lottery.constant.LotteryConstant;
import com.gitlab.vincenthung.gogotech.lottery.dao.DrawDAO;
import com.gitlab.vincenthung.gogotech.lottery.dao.TicketDAO;
import com.gitlab.vincenthung.gogotech.lottery.dto.DrawDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.TicketDTO;
import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.gitlab.vincenthung.gogotech.lottery.entity.Ticket;
import com.gitlab.vincenthung.gogotech.lottery.enumtype.DrawStatus;
import com.gitlab.vincenthung.gogotech.lottery.exception.ServiceException;
import com.gitlab.vincenthung.gogotech.lottery.service.LotteryService;
import com.gitlab.vincenthung.gogotech.lottery.service.NotificationService;
import com.gitlab.vincenthung.gogotech.lottery.utils.RandomUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LotteryServiceImpl implements LotteryService {

	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private DrawDAO drawDAO;

	@Autowired
	private NotificationService notificationService;

	@PostConstruct
	public void init() {
		Draw draw = drawDAO.getCurrentDraw();
		if (draw == null) {
			drawDAO.addNewDraw();
		}
	}

	@Override
	public TicketDTO purchaseTicket(Long contestantId) throws ServiceException {
		log.debug("purchaseTicket#start - contestantId=[{}]", contestantId);

		boolean success = false;

		try {

			if (LotteryConstant.lotteryProcessLock)
				throw new ServiceException(
						ServiceException.LOTTERY_PURCHASE_IN_DRAWING,
						"The current draw is going to draw the winner, no accepting new purchase");

			Draw draw = drawDAO.getCurrentDraw();
			if (!DrawStatus.ACCEPTING.equals(draw.getStatus()))
				throw new ServiceException(
						ServiceException.LOTTERY_PURCHASE_ISSUE_FAIL,
						"The latest draw is not accepting new purchases");

			Ticket ticket = ticketDAO.addTicket(contestantId, draw);

			success = true;
			return TicketDTO.fromEntity(ticket);

		} catch (DataAccessException e) {
			throw new ServiceException(
					ServiceException.DATABASE_ACCESS_ERROR,
					"System error - ticket issurance failed",
					e);
		} finally {
			log.debug("purchaseTicket#end - success=[{}]", success);
		}
	}

	@Override
	public DrawDTO enquiryDrawDetails(Long drawId) throws ServiceException {
		log.debug("enquiryDrawDetails#start - drawId=[{}]", drawId);

		boolean success = false;

		try {

			Draw draw = drawDAO.findById(drawId);

			if (draw == null)
				throw new ServiceException(
						ServiceException.LOTTERY_ENQUIRY_DRAW_NOT_EXIST,
						MessageFormat.format("No records for drawId=[{0}]", drawId));


			success = true;
			return DrawDTO.fromEntity(draw);

		} catch (DataAccessException e) {
			throw new ServiceException(
					ServiceException.DATABASE_ACCESS_ERROR,
					"System error - draw enquiry failed",
					e);
		} finally {
			log.debug("enquiryDrawDetails#end - success=[{}]", success);
		}
	}

	@Override
	@Scheduled(
			fixedDelayString = "${lottery.draw-interval-ms}",
			initialDelayString = "${lottery.draw-interval-ms}")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED, rollbackFor = Throwable.class)
	public void drawLotteryWinner() throws ServiceException {
		log.info("drawLotteryWinner#start");

		boolean success = false;
		Long winnerTickerId = null;

		try {

			// Gain the process lock
			if (LotteryConstant.lotteryProcessLock) {
				log.warn("LotteryConstant.lotteryProcessLock is locked");
				return;
			}

			LotteryConstant.lotteryProcessLock = true;
			log.debug("LotteryConstant.lotteryProcessLock gained");

			// Obtain the current draw
			Draw draw = drawDAO.getCurrentDraw();

			if (draw == null) {
				log.warn("current draw cannot be found");
				return;
			}

			// Finalize the draw
			if (DrawStatus.ACCEPTING.equals(draw.getStatus())) {
				Long drawId = draw.getId();
				List<Ticket> tickets = ticketDAO.listByDrawId(drawId);
				if (tickets == null || tickets.isEmpty()) {
					log.info("There are no tickets for drawId=[{}]", drawId);
					draw.setStatus(DrawStatus.CANCELLED);
					draw.setLastUpdateTime(new Date());
				} else {
					log.info("There are [{}] tickets for drawId=[{}]", tickets.size(), drawId);
					draw.setStatus(DrawStatus.FINALIZED);
					draw.setLastUpdateTime(new Date());

					// Draw the winner
					Ticket winnerTicket = tickets.get(RandomUtils.randomInt(tickets.size()));
					winnerTickerId = winnerTicket.getId();
					log.info("Winner ticket: [{}]", winnerTickerId);

					draw.setWinnerTicket(winnerTicket);

					// Send the notification to the winner
					notificationService.notifyWinner(drawId, winnerTickerId, winnerTicket.getContestantId());

				}

			} else {
				log.warn("current draw already finalized/cancelled");
			}

			// Create next draw
			drawDAO.addNewDraw();

			success = true;

		} catch (DataAccessException e) {
			log.error("Database access error during the scheduled job", e);
		} finally {
			// Release lock
			LotteryConstant.lotteryProcessLock = false;
			log.debug("LotteryConstant.lotteryProcessLock released");

			log.debug("drawLotteryWinner#end - success=[{}], winnerTickerId=[{}]",
					success, winnerTickerId);
		}
	}

}
