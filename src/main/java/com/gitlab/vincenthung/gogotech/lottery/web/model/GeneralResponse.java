package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * GeneralResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class GeneralResponse   {
  @JsonProperty("status")
  private String status = null;

  @JsonProperty("errCode")
  private String errCode = null;

  @JsonProperty("errDesc")
  private String errDesc = null;

  public GeneralResponse status(String status) {
    this.status = status;
    return this;
  }

  /**
   * The status of the request is processed normally, possible values: \"S\": Success, \"F\": Failure
   * @return status
  **/
  @ApiModelProperty(value = "The status of the request is processed normally, possible values: \"S\": Success, \"F\": Failure")


  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public GeneralResponse errCode(String errCode) {
    this.errCode = errCode;
    return this;
  }

  /**
   * Error code of the exception occurred during the request
   * @return errCode
  **/
  @ApiModelProperty(value = "Error code of the exception occurred during the request")


  public String getErrCode() {
    return errCode;
  }

  public void setErrCode(String errCode) {
    this.errCode = errCode;
  }

  public GeneralResponse errDesc(String errDesc) {
    this.errDesc = errDesc;
    return this;
  }

  /**
   * Error description of the exception occurred during the request
   * @return errDesc
  **/
  @ApiModelProperty(value = "Error description of the exception occurred during the request")


  public String getErrDesc() {
    return errDesc;
  }

  public void setErrDesc(String errDesc) {
    this.errDesc = errDesc;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GeneralResponse generalResponse = (GeneralResponse) o;
    return Objects.equals(this.status, generalResponse.status) &&
        Objects.equals(this.errCode, generalResponse.errCode) &&
        Objects.equals(this.errDesc, generalResponse.errDesc);
  }

  @Override
  public int hashCode() {
    return Objects.hash(status, errCode, errDesc);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GeneralResponse {\n");
    
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    errCode: ").append(toIndentedString(errCode)).append("\n");
    sb.append("    errDesc: ").append(toIndentedString(errDesc)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

