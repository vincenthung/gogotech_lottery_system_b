package com.gitlab.vincenthung.gogotech.lottery.dto.result;

import lombok.Data;

@Data
public class ResultDTO {

	private String status;
	private String errCode;
	private String errDesc;

}
