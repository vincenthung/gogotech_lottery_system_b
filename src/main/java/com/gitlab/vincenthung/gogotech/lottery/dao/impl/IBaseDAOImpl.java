package com.gitlab.vincenthung.gogotech.lottery.dao.impl;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;

@Transactional(readOnly = true)
public abstract class IBaseDAOImpl<T, ID extends Serializable>
extends GenericDAOImpl<T, ID>
implements GenericDAO<T, ID> {

	@Autowired
	private SessionFactory sessionFactory;

	@PostConstruct
	private void init() {
		setSessionFactory(sessionFactory);
	}

}
