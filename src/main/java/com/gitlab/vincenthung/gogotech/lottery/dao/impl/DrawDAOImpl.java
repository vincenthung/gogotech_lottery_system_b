package com.gitlab.vincenthung.gogotech.lottery.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.vincenthung.gogotech.lottery.dao.DrawDAO;
import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.gitlab.vincenthung.gogotech.lottery.enumtype.DrawStatus;
import com.googlecode.genericdao.search.Search;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class DrawDAOImpl extends IBaseDAOImpl<Draw, Long> implements DrawDAO {

	@Override
	public Draw findById(Long id) {
		return this.find(id);
	}

	@Override
	public Draw getCurrentDraw() {

		Search search = new Search();
		search.addSort("id", true);
		List<Draw> draws = search(search);
		if (draws == null || draws.isEmpty())
			return null;

		return draws.get(0);

	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.SUPPORTS, rollbackFor = Throwable.class)
	public Draw addNewDraw() {

		Draw draw = new Draw();

		Date now = new Date();

		draw.setStatus(DrawStatus.ACCEPTING);
		draw.setCreateTime(now);
		draw.setLastUpdateTime(now);

		save(draw);

		log.debug("Add new draw in database - drawId = [{}]", draw.getId());

		return draw;

	}

}
