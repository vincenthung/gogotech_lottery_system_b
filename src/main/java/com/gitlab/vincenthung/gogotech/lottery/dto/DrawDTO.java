package com.gitlab.vincenthung.gogotech.lottery.dto;

import java.util.Date;

import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.gitlab.vincenthung.gogotech.lottery.enumtype.DrawStatus;

import lombok.Data;

@Data
public class DrawDTO {

	private Long id;
	private Long winnerTicketId;
	private DrawStatus status;
	private Date createTime;
	private Date lastUpdateTime;

	public static DrawDTO fromEntity(Draw entity) {

		if (entity == null)
			return null;

		DrawDTO dto = new DrawDTO();
		dto.setId(entity.getId());
		dto.setStatus(entity.getStatus());
		dto.setCreateTime(entity.getCreateTime());
		dto.setLastUpdateTime(entity.getLastUpdateTime());

		if (entity.getWinnerTicket() != null)
			dto.setWinnerTicketId(entity.getWinnerTicket().getId());

		return dto;
	}

}
