package com.gitlab.vincenthung.gogotech.lottery.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.gitlab.vincenthung.gogotech.lottery.enumtype.DrawStatus;

import lombok.Data;

@Data
@Entity
@Table(name = "DRAW")
public class Draw {

	@Id
	@GeneratedValue(generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id")
	private Long id;

	@ManyToOne(optional = true)
	@JoinColumn(name = "winner_ticket_id", nullable = true)
	private Ticket winnerTicket;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private DrawStatus status;

	@Column(name = "create_time", nullable = false)
	private Date createTime;

	@Column(name = "last_update_time", nullable = false)
	private Date lastUpdateTime;

}
