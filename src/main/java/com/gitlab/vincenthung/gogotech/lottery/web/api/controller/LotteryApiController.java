package com.gitlab.vincenthung.gogotech.lottery.web.api.controller;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.gitlab.vincenthung.gogotech.lottery.biz.LotteryBizDelegate;
import com.gitlab.vincenthung.gogotech.lottery.dto.DrawDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.TicketDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.result.ResultDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.result.SingleResultDTO;
import com.gitlab.vincenthung.gogotech.lottery.web.api.LotteryApi;
import com.gitlab.vincenthung.gogotech.lottery.web.model.GeneralResponse;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryDrawDetails;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryEnquiryResponse;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryPurchaseRequest;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryPurchaseResponse;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryPurchaseResult;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LotteryApiController implements LotteryApi {

	@Autowired
	private LotteryBizDelegate lotteryBizDelegate;

	@Override
	public ResponseEntity<LotteryEnquiryResponse> enquiryLotteryResult(
			@NotNull @ApiParam(value = "Draw ID to be enquired of the result", required = true) @Valid @RequestParam(value = "drawId", required = true) Long drawId) {
		log.info("enquiryLotteryResult - drawId = [{}]", drawId);

		LotteryEnquiryResponse response = new LotteryEnquiryResponse();

		SingleResultDTO<DrawDTO> result = lotteryBizDelegate.enquiryLotteryResult(drawId);

		if (result != null) {
			processResult(result, response);
			if (result.getData() != null) {
				response.setDrawDetails(convert(result.getData()));
			}
		}

		return ResponseEntity.ok(response);
	}

	@Override
	public ResponseEntity<LotteryPurchaseResponse> purchaseLottery(
			@ApiParam(value = "Lottery Purchase Request" ,required=true )  @Valid @RequestBody LotteryPurchaseRequest request) {

		log.info("purchaseLottery - request = [{}]", request);

		LotteryPurchaseResponse response = new LotteryPurchaseResponse();

		SingleResultDTO<TicketDTO> result = lotteryBizDelegate.purchaseLotteryTicket(
				request.getContestantId(), request.getAuthentication());

		if (result != null) {
			processResult(result, response);
			if (result.getData() != null) {
				response.setPurchaseResult(convert(result.getData()));
			}
		}

		return ResponseEntity.ok(response);
	}

	protected void processResult(ResultDTO result, GeneralResponse response) {
		if (result != null && response != null) {
			response.setStatus(result.getStatus());
			response.setErrCode(result.getErrCode());
			response.setErrDesc(result.getErrDesc());
		}
	}

	protected LotteryPurchaseResult convert(TicketDTO dto) {
		LotteryPurchaseResult result = new LotteryPurchaseResult();

		if (dto.getDraw() != null) {
			result.setDrawId(dto.getDraw().getId());
		}
		result.setTicketId(dto.getId());
		return result;
	}

	protected LotteryDrawDetails convert(DrawDTO dto) {
		LotteryDrawDetails details = new LotteryDrawDetails();

		details.setDrawId(dto.getId());
		details.setDrawStatus(dto.getStatus().toString());
		details.setWinnerTicketId(dto.getWinnerTicketId());

		return details;
	}

}
