package com.gitlab.vincenthung.gogotech.lottery.service;

public interface NotificationService {

	public boolean notifyWinner(Long drawId, Long ticketId, Long contestantId);

}
