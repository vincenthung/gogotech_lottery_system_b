package com.gitlab.vincenthung.gogotech.lottery.utils;

public class RandomUtils {

	public static int randomInt(int maxExclusive) {
		double random = Math.random() * maxExclusive;
		return (int) Math.round(Math.floor(random));
	}

}
