package com.gitlab.vincenthung.gogotech.lottery.service;

import com.gitlab.vincenthung.gogotech.lottery.dto.DrawDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.TicketDTO;
import com.gitlab.vincenthung.gogotech.lottery.exception.ServiceException;

public interface LotteryService {

	public TicketDTO purchaseTicket(Long contestantId) throws ServiceException;

	public DrawDTO enquiryDrawDetails(Long drawId) throws ServiceException;

	public void drawLotteryWinner() throws ServiceException;

}
