package com.gitlab.vincenthung.gogotech.lottery.dto.result;

import lombok.Data;

@Data
public class SingleResultDTO<T> extends ResultDTO {

	private T data;

}
