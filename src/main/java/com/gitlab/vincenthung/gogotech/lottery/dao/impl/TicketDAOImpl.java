package com.gitlab.vincenthung.gogotech.lottery.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.gitlab.vincenthung.gogotech.lottery.dao.TicketDAO;
import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.gitlab.vincenthung.gogotech.lottery.entity.Ticket;
import com.googlecode.genericdao.search.Search;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class TicketDAOImpl extends IBaseDAOImpl<Ticket, Long> implements TicketDAO {

	@Override
	@Transactional(readOnly = false, rollbackFor = Throwable.class)
	public Ticket addTicket(Long contestantId, Draw draw) {

		if (contestantId == null || draw == null) {
			log.error("contestantId or draw cannot be null for addTicket");
			return null;
		}

		log.debug("add ticket for contestantId=[{}], draw.id=[{}]",
				contestantId, draw.getId());

		Ticket ticket = new Ticket();
		ticket.setContestantId(contestantId);
		ticket.setDraw(draw);
		ticket.setCreateTime(new Date());
		save(ticket);

		return ticket;
	}

	@Override
	public List<Ticket> listByDrawId(Long drawId) {

		if (drawId == null) {
			log.error("drawId cannot be null for listByDrawId");
			return null;
		}

		log.debug("list tickets for drawId=[{}]", drawId);

		Search search = new Search();
		search.addFilterEqual("draw.id", drawId);

		return search(search);

	}

}
