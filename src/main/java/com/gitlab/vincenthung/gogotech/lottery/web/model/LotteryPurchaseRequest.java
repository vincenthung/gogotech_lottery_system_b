package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LotteryPurchaseRequest
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class LotteryPurchaseRequest   {
  @JsonProperty("contestantId")
  private Long contestantId = null;

  @JsonProperty("authentication")
  private String authentication = null;

  public LotteryPurchaseRequest contestantId(Long contestantId) {
    this.contestantId = contestantId;
    return this;
  }

  /**
   * Contestant ID who purchase the ticket
   * @return contestantId
  **/
  @ApiModelProperty(required = true, value = "Contestant ID who purchase the ticket")
  @NotNull


  public Long getContestantId() {
    return contestantId;
  }

  public void setContestantId(Long contestantId) {
    this.contestantId = contestantId;
  }

  public LotteryPurchaseRequest authentication(String authentication) {
    this.authentication = authentication;
    return this;
  }

  /**
   * Contestant's secret or JWT (if applicable)
   * @return authentication
  **/
  @ApiModelProperty(required = true, value = "Contestant's secret or JWT (if applicable)")
  @NotNull


  public String getAuthentication() {
    return authentication;
  }

  public void setAuthentication(String authentication) {
    this.authentication = authentication;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotteryPurchaseRequest lotteryPurchaseRequest = (LotteryPurchaseRequest) o;
    return Objects.equals(this.contestantId, lotteryPurchaseRequest.contestantId) &&
        Objects.equals(this.authentication, lotteryPurchaseRequest.authentication);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contestantId, authentication);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotteryPurchaseRequest {\n");
    
    sb.append("    contestantId: ").append(toIndentedString(contestantId)).append("\n");
    sb.append("    authentication: ").append(toIndentedString(authentication)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

