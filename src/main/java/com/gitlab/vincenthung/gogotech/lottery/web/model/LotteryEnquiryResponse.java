package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.gitlab.vincenthung.gogotech.lottery.web.model.GeneralResponse;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryDrawDetails;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LotteryEnquiryResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class LotteryEnquiryResponse extends GeneralResponse  {
  @JsonProperty("drawDetails")
  private LotteryDrawDetails drawDetails = null;

  public LotteryEnquiryResponse drawDetails(LotteryDrawDetails drawDetails) {
    this.drawDetails = drawDetails;
    return this;
  }

  /**
   * Get drawDetails
   * @return drawDetails
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LotteryDrawDetails getDrawDetails() {
    return drawDetails;
  }

  public void setDrawDetails(LotteryDrawDetails drawDetails) {
    this.drawDetails = drawDetails;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotteryEnquiryResponse lotteryEnquiryResponse = (LotteryEnquiryResponse) o;
    return Objects.equals(this.drawDetails, lotteryEnquiryResponse.drawDetails) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(drawDetails, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotteryEnquiryResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    drawDetails: ").append(toIndentedString(drawDetails)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

