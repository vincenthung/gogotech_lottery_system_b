package com.gitlab.vincenthung.gogotech.lottery.dao;

import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface DrawDAO extends GenericDAO<Draw, Long> {

	public Draw findById(Long id);

	public Draw getCurrentDraw();

	public Draw addNewDraw();

}
