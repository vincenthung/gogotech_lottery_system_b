package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.gitlab.vincenthung.gogotech.lottery.web.model.GeneralResponse;
import com.gitlab.vincenthung.gogotech.lottery.web.model.LotteryPurchaseResult;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LotteryPurchaseResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class LotteryPurchaseResponse extends GeneralResponse  {
  @JsonProperty("purchaseResult")
  private LotteryPurchaseResult purchaseResult = null;

  public LotteryPurchaseResponse purchaseResult(LotteryPurchaseResult purchaseResult) {
    this.purchaseResult = purchaseResult;
    return this;
  }

  /**
   * Get purchaseResult
   * @return purchaseResult
  **/
  @ApiModelProperty(value = "")

  @Valid

  public LotteryPurchaseResult getPurchaseResult() {
    return purchaseResult;
  }

  public void setPurchaseResult(LotteryPurchaseResult purchaseResult) {
    this.purchaseResult = purchaseResult;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotteryPurchaseResponse lotteryPurchaseResponse = (LotteryPurchaseResponse) o;
    return Objects.equals(this.purchaseResult, lotteryPurchaseResponse.purchaseResult) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(purchaseResult, super.hashCode());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotteryPurchaseResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    purchaseResult: ").append(toIndentedString(purchaseResult)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

