package com.gitlab.vincenthung.gogotech.lottery.biz;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitlab.vincenthung.gogotech.lottery.constant.ResultStatus;
import com.gitlab.vincenthung.gogotech.lottery.dto.DrawDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.TicketDTO;
import com.gitlab.vincenthung.gogotech.lottery.dto.result.SingleResultDTO;
import com.gitlab.vincenthung.gogotech.lottery.exception.ServiceException;
import com.gitlab.vincenthung.gogotech.lottery.service.LotteryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class LotteryBizDelegate {

	@Autowired
	private LotteryService lotteryService;

	public SingleResultDTO<TicketDTO> purchaseLotteryTicket(
			Long contestantId, String authentication) {

		log.info("purchaseLotteryTicket#start - contestantId = [{}], authentication = [{}]",
				contestantId, authentication);

		SingleResultDTO<TicketDTO> result = new SingleResultDTO<>();

		try {

			// Validate request
			if (contestantId == null)
				throw new ServiceException(
						ServiceException.LOTTERY_PURCHASE_REQ_INVALID,
						"contestantId must be presented as integer");

			// Skipped validate authentication on the request

			TicketDTO ticket = lotteryService.purchaseTicket(contestantId);

			result.setData(ticket);
			result.setStatus(ResultStatus.SUCCESS);

		} catch (ServiceException se) {
			result.setStatus(ResultStatus.FAILURE);
			result.setErrCode(se.getCode());
			result.setErrDesc(se.getDesc());
		} catch (Exception e) {
			result.setStatus(ResultStatus.FAILURE);
			result.setErrCode(ServiceException.UNEXPECTED_ERROR);
			result.setErrDesc("Unexpected error");
			log.debug("Unexpected error: {}", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("purchaseLotteryTicket#end - result = [{}]",
					result);
		}

		return result;
	}

	public SingleResultDTO<DrawDTO> enquiryLotteryResult(
			Long drawId) {

		log.info("enquiryLotteryResult#start - drawId = [{}]",
				drawId);

		SingleResultDTO<DrawDTO> result = new SingleResultDTO<>();

		try {

			// Validate request
			if (drawId == null)
				throw new ServiceException(
						ServiceException.LOTTERY_ENQUIRY_DRAW_INVALID,
						"drawId must be presented as integer");

			DrawDTO draw = lotteryService.enquiryDrawDetails(drawId);

			result.setData(draw);
			result.setStatus(ResultStatus.SUCCESS);

		} catch (ServiceException se) {
			result.setStatus(ResultStatus.FAILURE);
			result.setErrCode(se.getCode());
			result.setErrDesc(se.getDesc());
		} catch (Exception e) {
			result.setStatus(ResultStatus.FAILURE);
			result.setErrCode(ServiceException.UNEXPECTED_ERROR);
			result.setErrDesc("Unexpected error");
			log.debug("Unexpected error: {}", e.getMessage());
			e.printStackTrace();
		} finally {
			log.info("enquiryLotteryResult#end - result = [{}]",
					result);
		}

		return result;
	}

}
