package com.gitlab.vincenthung.gogotech.lottery.exception;

import java.text.MessageFormat;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceException extends Exception {

	private static final long serialVersionUID = 6255894532785444130L;

	public static final String DATABASE_ACCESS_ERROR = "G001";
	public static final String UNEXPECTED_ERROR = "G999";

	public static final String LOTTERY_PURCHASE_REQ_INVALID = "L001";
	public static final String LOTTERY_PURCHASE_IN_DRAWING = "L002";
	public static final String LOTTERY_PURCHASE_ISSUE_FAIL = "L003";

	public static final String LOTTERY_ENQUIRY_DRAW_INVALID	= "L004";
	public static final String LOTTERY_ENQUIRY_DRAW_NOT_EXIST = "L005";


	private String code;
	private String desc;
	private Throwable cause;

	public ServiceException() {
		this(UNEXPECTED_ERROR, "Unexpceted error");
	}

	public ServiceException(String code, String desc) {
		this(code, desc, null);
	}

	public ServiceException(String code, String desc, Throwable cause) {
		this.code = code;
		this.desc = desc;
		this.cause = cause;
	}

	@Override
	public String toString() {
		String message = MessageFormat.format("[{0}] {1}", code, desc);
		if (cause != null)
			message = message + ", caused by: " + cause.getMessage();
		return message;
	}

}
