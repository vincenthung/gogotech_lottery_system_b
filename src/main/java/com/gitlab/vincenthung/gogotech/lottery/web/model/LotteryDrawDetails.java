package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LotteryDrawDetails
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class LotteryDrawDetails   {
  @JsonProperty("drawId")
  private Long drawId = null;

  @JsonProperty("winnerTicketId")
  private Long winnerTicketId = null;

  @JsonProperty("drawStatus")
  private String drawStatus = null;

  public LotteryDrawDetails drawId(Long drawId) {
    this.drawId = drawId;
    return this;
  }

  /**
   * Draw ID enquired
   * @return drawId
  **/
  @ApiModelProperty(value = "Draw ID enquired")


  public Long getDrawId() {
    return drawId;
  }

  public void setDrawId(Long drawId) {
    this.drawId = drawId;
  }

  public LotteryDrawDetails winnerTicketId(Long winnerTicketId) {
    this.winnerTicketId = winnerTicketId;
    return this;
  }

  /**
   * ID of the ticket drawed as winner, presented when drawStatus is \"FINALIZED\"
   * @return winnerTicketId
  **/
  @ApiModelProperty(value = "ID of the ticket drawed as winner, presented when drawStatus is \"FINALIZED\"")


  public Long getWinnerTicketId() {
    return winnerTicketId;
  }

  public void setWinnerTicketId(Long winnerTicketId) {
    this.winnerTicketId = winnerTicketId;
  }

  public LotteryDrawDetails drawStatus(String drawStatus) {
    this.drawStatus = drawStatus;
    return this;
  }

  /**
   * Draw status, possible values: \"FINALIZED\": The winner of the draw is finalized, \"CANCELLED\": The draw is cancelled as no valid tickets for the draw, \"ACCEPTING\": The draw is accepting for ticket purchases
   * @return drawStatus
  **/
  @ApiModelProperty(value = "Draw status, possible values: \"FINALIZED\": The winner of the draw is finalized, \"CANCELLED\": The draw is cancelled as no valid tickets for the draw, \"ACCEPTING\": The draw is accepting for ticket purchases")


  public String getDrawStatus() {
    return drawStatus;
  }

  public void setDrawStatus(String drawStatus) {
    this.drawStatus = drawStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotteryDrawDetails lotteryDrawDetails = (LotteryDrawDetails) o;
    return Objects.equals(this.drawId, lotteryDrawDetails.drawId) &&
        Objects.equals(this.winnerTicketId, lotteryDrawDetails.winnerTicketId) &&
        Objects.equals(this.drawStatus, lotteryDrawDetails.drawStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(drawId, winnerTicketId, drawStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotteryDrawDetails {\n");
    
    sb.append("    drawId: ").append(toIndentedString(drawId)).append("\n");
    sb.append("    winnerTicketId: ").append(toIndentedString(winnerTicketId)).append("\n");
    sb.append("    drawStatus: ").append(toIndentedString(drawStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

