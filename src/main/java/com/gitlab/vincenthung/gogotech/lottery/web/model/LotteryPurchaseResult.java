package com.gitlab.vincenthung.gogotech.lottery.web.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * LotteryPurchaseResult
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2022-01-09T14:51:31.369+08:00")




public class LotteryPurchaseResult   {
  @JsonProperty("drawId")
  private Long drawId = null;

  @JsonProperty("ticketId")
  private Long ticketId = null;

  public LotteryPurchaseResult drawId(Long drawId) {
    this.drawId = drawId;
    return this;
  }

  /**
   * Draw ID of the ticket
   * @return drawId
  **/
  @ApiModelProperty(value = "Draw ID of the ticket")


  public Long getDrawId() {
    return drawId;
  }

  public void setDrawId(Long drawId) {
    this.drawId = drawId;
  }

  public LotteryPurchaseResult ticketId(Long ticketId) {
    this.ticketId = ticketId;
    return this;
  }

  /**
   * ID of the ticket
   * @return ticketId
  **/
  @ApiModelProperty(value = "ID of the ticket")


  public Long getTicketId() {
    return ticketId;
  }

  public void setTicketId(Long ticketId) {
    this.ticketId = ticketId;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LotteryPurchaseResult lotteryPurchaseResult = (LotteryPurchaseResult) o;
    return Objects.equals(this.drawId, lotteryPurchaseResult.drawId) &&
        Objects.equals(this.ticketId, lotteryPurchaseResult.ticketId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(drawId, ticketId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LotteryPurchaseResult {\n");
    
    sb.append("    drawId: ").append(toIndentedString(drawId)).append("\n");
    sb.append("    ticketId: ").append(toIndentedString(ticketId)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

