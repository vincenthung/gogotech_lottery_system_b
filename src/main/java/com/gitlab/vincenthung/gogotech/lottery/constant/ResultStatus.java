package com.gitlab.vincenthung.gogotech.lottery.constant;

public class ResultStatus {

	public static final String SUCCESS = "S";
	public static final String FAILURE = "F";

}
