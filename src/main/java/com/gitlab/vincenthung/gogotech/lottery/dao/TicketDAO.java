package com.gitlab.vincenthung.gogotech.lottery.dao;

import java.util.List;

import com.gitlab.vincenthung.gogotech.lottery.entity.Draw;
import com.gitlab.vincenthung.gogotech.lottery.entity.Ticket;
import com.googlecode.genericdao.dao.hibernate.GenericDAO;

public interface TicketDAO extends GenericDAO<Ticket, Long> {

	public Ticket addTicket(Long contestantId, Draw draw);

	public List<Ticket> listByDrawId(Long drawId);

}
