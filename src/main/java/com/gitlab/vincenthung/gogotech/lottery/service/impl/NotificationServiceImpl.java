package com.gitlab.vincenthung.gogotech.lottery.service.impl;

import org.springframework.stereotype.Service;

import com.gitlab.vincenthung.gogotech.lottery.service.NotificationService;

@Service
public class NotificationServiceImpl implements NotificationService {

	@Override
	public boolean notifyWinner(Long drawId, Long ticketId, Long contestantId) {
		// Assuming to call Core-Service for notifying the winner
		return true;
	}

}
