package com.gitlab.vincenthung.gogotech.lottery.enumtype;

public enum DrawStatus {

	ACCEPTING,
	CANCELLED,
	FINALIZED,
	;

}
