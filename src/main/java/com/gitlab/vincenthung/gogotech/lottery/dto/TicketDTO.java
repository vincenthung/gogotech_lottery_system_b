package com.gitlab.vincenthung.gogotech.lottery.dto;

import java.util.Date;

import com.gitlab.vincenthung.gogotech.lottery.entity.Ticket;

import lombok.Data;

@Data
public class TicketDTO {

	private Long id;
	private Long contestantId;
	private DrawDTO draw;
	private Date createTime;

	public static TicketDTO fromEntity(Ticket entity) {

		TicketDTO dto = new TicketDTO();
		dto.setId(entity.getId());
		dto.setContestantId(entity.getContestantId());
		dto.setCreateTime(entity.getCreateTime());

		if (entity.getDraw() != null)
			dto.setDraw(DrawDTO.fromEntity(entity.getDraw()));

		return dto;
	}

}
