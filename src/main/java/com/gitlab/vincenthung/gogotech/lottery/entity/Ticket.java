package com.gitlab.vincenthung.gogotech.lottery.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

@Data
@Entity
@Table(name = "TICKET")
public class Ticket {

	@Id
	@GeneratedValue(generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "id")
	private Long id;

	@Column(name = "contestant_id")
	private Long contestantId;

	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "draw_id", nullable = false)
	private Draw draw;

	@Column(name = "create_time", nullable = false)
	private Date createTime;

}
