# gogotech_lottery_system_b

This is the implementation of the lottery system as the requirement of the test descripted in https://gogox.notion.site/Code-Test-Lottery-System-B-d71a5c71feef43a6998c7caacb598b12

This test is implemented as a standalone micro-service of Spring boot with Java 8

## Description

This is the implementation on the lottery system, the following main features are implemented

### APIs

- /lottery/purchase
- /lottery/result?drawId={$drawId}

### Schedulers

- LotteryDrawScheduler

## Roadmap

1. Designing the data structures for this lottery system
2. Designing the Restful API to accept front-end requests
3. Drawing the normal flow for implementing the logics & considering about the exceptions
4. Implementing the Restful API and underlying logics according the flow
5. Test the application with test plan

## Data Structures

![Data Structures](/images/Data_Structures.drawio.png)

## API Definitions

[Go to YAML Definitions](/resources/yaml/gogotech_lottery_system.yaml)

## Logical Flow

- ### **/lottery/purchase**
  ![Logical Flow - Purchase Lottery](/images/Logical_Flow_Purchase_Lottery.drawio.png)

- ### **/lottery/result**
  ![Logical Flow - Lottery Result](/images/Logical_Flow_Lottery_Result.drawio.png)

- ### **LotteryDrawScheduler**
  ![Logical Flow - Lottery Draw Scheduler](/images/Logical_Flow_Lottery_Draw_Scheduler.drawio.png)

## Test Plan

[Go to Postman Requests Collection JSON](/tests/GOGOTECH%20-%20Lottery%20System.postman_collection.json)

## Execution

Run run.sh (Linux with Java 8) or run.bat (Windows) with the pre-built micro-service.

## Limitations

These of the following limitations are considered to be not implemented in this test due to the time and complexities.

### System-wise

- Not a distributable service, and not load-balanceable due to the lock of the drawing process is locked locally

### Functional-wise

- No contestants information in this test or notification method to the winner
- Not checking the existing requests of purchase ticket due to time constriant
- Not supported for compentants management / administration functions (register, update etc.)
- Not supported for JWT on user authentication on the API requests, instead of using the user's secret directly in the requests
- Progress-lock using NoSQL (i.e. Redis) for the business logically flow safety on drawing the winner, instead of using static variables
